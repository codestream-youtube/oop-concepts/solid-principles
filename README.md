# SOLID Prinsipləri

> Hər prinsip üçün ayrıca branch yaradılıb, branch-ları dəyişməklə hər prinsip üçün olan nümunələrə baxa bilərsiniz

## 1 - Single Responsibility Prinsipi (branch: single-responsibility)

Hər bir class sistemin yalnız bir funksionallığına və ya tək hissəsinə cavabdeh olmalıdır. Qısaca olaraq, class-ın
yalnız 1 dəyişmə səbəbi olmalıdır.
Funksionallıqları class-lara ayırmaq bizə bir çox üstünlük verir.

### Üstünlükləri

* Class-ın təkrar istifadəsi
* Problemin qısa müddətdə tapılması və həlli
* və s.

## 2 - Open-Closed Prinsipi (branch: open-closed)

Proqramın komponentləri genişlənməyə açıq, dəyişikliyə qapalı olmalıdır. Komponent dedikdə class-lar, modullar, metodlar
nəzərdə tutulur.

### Üstünlükləri

* Kodun daha asan genişlənmə imkanı (Easier Extensibility)
* Kodun daha yaxşı dəstəklənməsi (Easier to Maintain)
* Flexibility.

## 3 - Liskov Substitution Prinsipi (branch: liskov-substitution)

Alt tiplər daha üst tiplərini əvəzləyə bilməlidir. Subclass öz parent class-ını əvəzləyə bilməlidir və bu zaman “kod
qırılması” baş verməməlidir. Əvəzləmək o deməkdir ki, alt tip özünü üst tip kimi “aparmalıdır”.
Yəni nəinki üst tipin metodlarını istifadə edə bilər , bu zaman üst tip necə davranmalıydısa, elə davranmalıdır.

### Üstünlükləri

* Kodun genişlənməsindən sonra klient kodun istifadəsində yara biləcək problemləri öncədən əngəlləyir.

## 4 - Interface Segregation Principle (branch: interface-segregation)

Interface implement edən kodu istifadə etməyəcəyi/edə bilməyəcəyi metodlardan asılı olmamalıdır.

### Üstünlükləri

* Mənasız və lazımsız kodun yazılmaması və daha qısa class faylının rahat dəstəklənməsi
* Liskov substitution prinsipinin pozulmasının qarşısını alır
* və s.